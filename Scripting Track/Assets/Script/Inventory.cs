﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour, ICraftable
{
    //weapons represents weapons in inventory with element location representing location
    List<Weapon> weapons = new List<Weapon>();

    //weaponMap is used for getting and setting values for each weapon
    Dictionary<string, Weapon> weaponMap = new Dictionary<string, Weapon>();

    //for accessing item class
    Item<Weapon> nextItem = new Item<Weapon>();

    //to make sure steps done in correct order
    private int count = 0;

    private void Start()
    {
        print("This is your inventory. Put this training sword in it by pressing space.");
        UserInput.OnPress += RecievePress;
    }

    //will simulate inventory mechanics after start
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && count == 0)
        {
            Weapon starter1 = new Sword("Short sword", 15);
            nextItem.UpdateItem(starter1);
            addItem(starter1);

            Weapon starter2 = new Bow("Hunting bow", 25);
            Weapon starter3 = new Staff("Bamboo Staff", 10);
            count++;

            print("Good work. I have put some other weapons in for you. Press 'i' to view all the weapons in your inventory.");

            addItem(starter2);
            addItem(starter3);
        }

        else if (Input.GetKeyDown(KeyCode.I) && count == 1)
        {
            viewInventory();
            count++;

            print("Lets see which weapon is stronger between the bow and the staff. Press a number to view the corresponding weapon's stats (Press 2).");
        }

        else if (Input.GetKeyDown(KeyCode.Alpha2) && count == 2)
        {
            print(displayStats(1));
            count++;

            print("If you press q, you can compare this weapon to another weapon.");
        }

        else if (Input.GetKeyDown(KeyCode.Q) && count == 3)
        {
            count++;

            print("Now press 3 to compare this weapon to the third weapon.");
        }

        else if (Input.GetKeyDown(KeyCode.Alpha3) && count == 4)
        {
            int i = weapons[1].CompareTo(weapons[2]);
            count++;

             if (i == -1)
            {
                print("The first weapon is weaker than the second weapon");
            }

            else if (i == 1)
            {
                print("The first weapon is stronger than the second weapon");
            }

            else
            {
                print("The first weapon is equal to the second weapon");
            }

            print("Now press 3 and then d to discard the third weapon for parts.");
        }

        else if (Input.GetKeyDown(KeyCode.Alpha3) && count == 5)
        {
            count++;
        }

        else if (Input.GetKeyDown(KeyCode.D) && count == 6)
        {
            count++;
            dismantle(2);

            iterateMap();
            print("Good job. Tutorial complete.");
        }
    }

    //testing for now, will become method for displaying values of all weapons in map
    public void iterateMap()
    {
        //iterate through the Dictionary for testing
        foreach (KeyValuePair<string, Weapon> weapon in weaponMap)
        {
            Weapon temp = weapon.Value;
            print(weapon.Key + " with stats: " + " Power: " + temp.Power + " Type: " + temp.Type);
        }
    }

    public void viewInventory()
    {
        string s1 = "Inventory: ";
        string body = "";

        if (weapons.Count < 1)
        {
            print("Inventory is empty");
        }

        else {
            foreach (Weapon weapon in weapons)
            {
                body += weapon.Name + ", ";
            }

            print(s1 + body);
        }
    }

    //reutrn stats of given weapon only in inventory
    public string displayStats(int i)
    {
        if (i > weapons.Count)
        {
            return "There is no item in this slot";
        }
        else if (weapons.Count == 0)
        {
            return "Inventory is empty";
        }

        Weapon tmp = weapons[i];
        string name = tmp.Name;
        int power = tmp.Power;
        string type = tmp.Type;

        return "Name: " + name + " Power: " + power + " Type: " + type;
    }
    
    //helper for adding item to List
    public void addItem(Weapon item)
    {
        weapons.Add(item);
        weaponMap.Add(item.Name, item);
    }

    public void dismantle(int i)
    {
        weaponMap.Remove(weapons[i].Name);
        weapons.Remove(weapons[i]);
        
    }

    public void RecievePress(KeyCode keyDown)
    {
        print(keyDown.ToString());
    }
}
