﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Sword : Weapon

{
    public Sword(string n, int p) : base(n, p)
    {
        type = "Sword";
    }


}
