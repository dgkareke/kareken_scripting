﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Staff : Weapon
{
    public Staff(string n, int p) : base(n, p)
    {
        type = "Staff";
    }
}
