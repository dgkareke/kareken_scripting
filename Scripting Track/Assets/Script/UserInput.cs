﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this class if for getting user inputs from the keyboard and simulating interaction with the Inventory
public class UserInput : MonoBehaviour
{
    public delegate void ButtonPress(KeyCode keyDown);
    public static event ButtonPress OnPress;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnPress(KeyCode.Space);
        }

        else if (Input.GetKeyDown(KeyCode.I))
        {
            OnPress(KeyCode.I);
        }
    }
}
