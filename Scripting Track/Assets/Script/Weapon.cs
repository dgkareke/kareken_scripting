﻿using System.Collections;
using System;
using UnityEngine;

public class Weapon : IComparable<Weapon>
{
    protected string name;
    [Range(1, 100)] protected int power;
    protected string type;
    
    public Weapon (string n, int p)
    {
        name = n;
        power = p;
    }

    public int CompareTo(Weapon other)
    {
        if (power < other.power)
        {
            return -1;
        }

        else if (power == other.power)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

    //Properties
    public string Name
    {
        get {
            return name;
        }

        set {
            name = value;
        }
    }

    public int Power
    {
        get
        {
            return power;
        }

        set
        {
            power = value;
        }
    }

    public string Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
        }
    }
}
