﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : Weapon
{
    public Bow(string n, int p) : base(n, p)
    {
        type = "Bow";
    }

}
